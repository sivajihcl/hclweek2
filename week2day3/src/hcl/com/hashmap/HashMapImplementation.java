package hcl.com.hashmap;

import java.util.*;
public class HashMapImplementation {
	public static void main(String[] args) {
		HashMap<Integer,String> hash =new HashMap<Integer,String>();
		hash.put(11,"Anil");
		hash.put(64, "mani");
		hash.put(32,"kiran");
		hash.put(81, "ram");
		System.out.println(hash);
		System.out.println(hash.hashCode());
		System.out.println(hash.size());
		Set<Integer> key = hash.keySet();
		for(int i:key) {
			if(i==1) {
				System.out.println("Key present");
			}
		}
		System.out.println(hash.values());
		System.out.println(key);
		System.out.println(hash.get(2));
		System.out.println(hash.containsValue("kishore"));
		System.out.println(hash);
		
		for(Map.Entry<Integer,String> entry : hash.entrySet())
			System.out.println(entry.getKey() + entry.getValue());
	}
}