package customexception;

import java.util.ArrayList;

import java.util.Arrays;

public class CustomException extends Exception {
	public CustomException(String str) {
		super(str);
		}
}
class Main {
	
	ArrayList<String> FruitChecker = new ArrayList<>(Arrays.asList("banana", "grap", "kiwi"));
	
	public void checkLanguage(String Fruit) throws CustomException {
		if(FruitChecker.contains(Fruit)) {
			throw new CustomException(Fruit + " already exists");
			}
		else {
			FruitChecker.add(Fruit);
			System.out.println(Fruit + " is added to the ArrayList");
			}
	}

	public static void main(String[] args) {
			Main obj = new Main();
			try {
				obj.checkLanguage("Pinapple");
				obj.checkLanguage("banana");
				}
			catch(CustomException e) {
				System.out.println("[" + e + "] Exception Occured");
				}
			}
	}
