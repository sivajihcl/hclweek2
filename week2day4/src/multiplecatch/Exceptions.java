package multiplecatch;

public class Exceptions {

	public static void main(String[] args) {
		try {
			int a[]=new int[5];
			a[5]=25/0;
		} 
		catch(ArithmeticException ae)
		{
			System.out.println("Arithmetic Exception");
		}
		 catch(ArrayIndexOutOfBoundsException be)
		{
			System.out.println("Array Index Bounded"); 
		 }
		 catch(Exception e) {
			 System.out.println("Close the code");
		 }
		}
}
