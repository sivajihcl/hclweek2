package trycatch;

import java.util.Scanner;

public class NestedTryException {

	public static void main(String[] args) {
		System.out.println("Hello");
		System.out.println("Enter name");
		
		Scanner s= new Scanner(System.in);
		int var=s.nextInt();
		System.out.println(var);
		try{
			int a[]=new int[4];
			a[4]=20/var;
			}
		catch(ArithmeticException e)
		{
			System.err.println("Arithmetic");
			}
		catch(ArrayIndexOutOfBoundsException e)
		{
			System.err.println("Array Index");
			}
		catch(Exception e)
		{
			System.err.println("Exception Handle");
			}
		System.out.println(" Thank you");
		s.close();
	}
}
