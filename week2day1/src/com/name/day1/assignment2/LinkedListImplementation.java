package com.name.day1.assignment2;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Scanner;

public class LinkedListImplementation {

	public static void main(String[] args) {
		//create a linked list
		LinkedList<Integer>list = new LinkedList<Integer>();
	
		//create a scanner object
		Scanner scanner = new Scanner(System.in);
		//code to check whether the input is an integer or not 
		//simultaneously add the no in the linked list
		System.out.println("number is");
		for(int i=0;i<5;i++)
		{
			list.add(scanner.nextInt());
		}
		System.out.println("LinkedList" + list);
		Collections.sort(list);
		System.out.println(list);
		list.sort(Collections.reverseOrder());
		System.out.println(list);

		}

	}
