package com.name.day1.assignment1;

import java.util.ArrayList;

public class ArrayListImplementation {

	public static void main(String[] args) {
		//Create and  array List object
		ArrayList<Integer> list = new ArrayList<Integer>();
		//add elements in the arrayList
		list.add(10);
		list.add(20);
		list.add(50);
		list.add(40);
		list.add(20);
		// print the size of the list
		System.out.println(list.size());
		// print the minimum value present of the list
		int min = list.get(0);
		for(int i=1;i<list.size();i++) {
			if(min>list.get(i)) {
				min = list.get(i);
			}
		}
		System.out.println(min);
		//checking whether arrayList is empty or not
		System.out.println(list.isEmpty());
		//print the index of any element
		System.out.println(list.get(3));
		//remove the value , by passing index
		System.out.println(list.remove(0));
		// remove the value, by passing the value 
		System.out.println(list.remove(1));
		//display the arrayList using forEach loop
		System.out.println("list values");
		for(int al:list) {
			System.out.println(al);
		}
	}

}
