package com.name.day1.assignment3;

import java.util.*;

public class StackImplementation {

	public static void main(String[] args) {
		// Create an object of Stack of type String
		Stack<String> s = new Stack<>();
		// add elements into the Stack 
		s.push("siva");
		s.push("anil");
		s.push("mani");
		s.push("kiran");
		// Display the Stack 
		System.out.println(s);
		// Create an iterator
		Iterator i = s.iterator();
		while(i.hasNext()) {
			System.out.println(i.next());
		}

	}

}
