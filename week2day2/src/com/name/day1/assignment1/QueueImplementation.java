package com.name.day1.assignment1;

import java.util.PriorityQueue;

public class QueueImplementation {

	public static void main(String[] args) {
		PriorityQueue<Integer> qi = new PriorityQueue<Integer>();
		qi.add(10);
		qi.add(15);
		qi.add(32);
		qi.add(28);
		System.out.println("priorityQueue values");
		
		System.out.println(qi);
		System.out.println(qi.peek());
		System.out.println(qi.size());
		System.out.println(qi.element());
		System.out.println(qi.remove(10));
		System.out.println(qi);
		System.out.println(qi.removeAll(qi));
		System.out.println(qi.isEmpty());
		
		

	}

}
