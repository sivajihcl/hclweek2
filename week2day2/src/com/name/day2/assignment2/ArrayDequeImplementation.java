package com.name.day2.assignment2;

import java.util.ArrayDeque;

import java.util.Deque;

public class ArrayDequeImplementation {

	public static void main(String[] args) {
		// Create deque object using ArrayDeque Class

		Deque<Integer> deque = new ArrayDeque<Integer>();
		// add elements to the deque
		deque.add(27);
		deque.add(46);
		deque.add(52);
		deque.add(13);
		System.out.println(deque);
		// insert an element at the head
		deque.addFirst(89);
		// insert an element at the tail
		deque.addLast(22);
		// print the deque
		System.out.println(deque);
		//peek the first element
		deque.peekFirst();
		System.out.println(deque);
		//delete all the elements of deque
		deque.clear();
		System.out.println(deque);

	}

}
