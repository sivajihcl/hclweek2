package com.name.day2.assignment3;

import java.util.TreeSet;

public class TreeSetImplemtation {

	public static void main(String[] args) {
		// create a treeSet object
		TreeSet<Integer> tree = new TreeSet<Integer>();
		// add elements to treeSet object
		tree.add(95);
		tree.add(23);
		tree.add(45);
		tree.add(15);
		// print the treeSet
		System.out.println(tree);
	}

}
